#pragma once

#include <stdio.h>

#define PGL_LOG(...) do {fprintf(stderr, __VA_ARGS__); fputs("\n", stderr);} while(0)
