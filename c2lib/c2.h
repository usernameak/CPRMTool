#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
void c2_init();
uint64_t c2_enc(uint64_t code, uint64_t key);
uint64_t c2_dec(uint64_t code, uint64_t key);
uint64_t c2_g(uint64_t code, uint64_t key);
void c2_ecbc(void *p_buffer, uint64_t key, int length);
void c2_dcbc(void *p_buffer, uint64_t key, int length);
#ifdef __cplusplus
}
#endif
