#include "c2.h"

#include <stdint.h>
#include "bswap.h"

uint32_t sbox_f[256];

static uint8_t sbox[256] = {
	0x3a, 0xd0, 0x9a, 0xb6, 0xf5, 0xc1, 0x16, 0xb7, 
	0x58, 0xf6, 0xed, 0xe6, 0xd9, 0x8c, 0x57, 0xfc, 
	0xfd, 0x4b, 0x9b, 0x47, 0x0e, 0x8e, 0xff, 0xf3, 
	0xbb, 0xba, 0x0a, 0x80, 0x15, 0xd7, 0x2b, 0x36, 
	0x6a, 0x43, 0x5a, 0x89, 0xb4, 0x5d, 0x71, 0x19, 
	0x8f, 0xa0, 0x88, 0xb8, 0xe8, 0x8a, 0xc3, 0xae, 
	0x7c, 0x4e, 0x3d, 0xb5, 0x96, 0xcc, 0x21, 0x00, 
	0x1a, 0x6b, 0x12, 0xdb, 0x1f, 0xe4, 0x11, 0x9d, 
	0xd3, 0x93, 0x68, 0xb0, 0x7f, 0x3b, 0x52, 0xb9, 
	0x94, 0xdd, 0xa5, 0x1b, 0x46, 0x60, 0x31, 0xec, 
	0xc9, 0xf8, 0xe9, 0x5e, 0x13, 0x98, 0xbf, 0x27, 
	0x56, 0x08, 0x91, 0xe3, 0x6f, 0x20, 0x40, 0xb2, 
	0x2c, 0xce, 0x02, 0x10, 0xe0, 0x18, 0xd5, 0x6c, 
	0xde, 0xcd, 0x87, 0x79, 0xaf, 0xa9, 0x26, 0x50, 
	0xf2, 0x33, 0x92, 0x6e, 0xc0, 0x3f, 0x39, 0x41, 
	0xaa, 0x5b, 0x7d, 0x24, 0x03, 0xd6, 0x2f, 0xeb, 
	0x0b, 0x99, 0x86, 0x4c, 0x51, 0x45, 0x8d, 0x2e, 
	0xef, 0x07, 0x7b, 0xe2, 0x4d, 0x7a, 0xfe, 0x25, 
	0x5c, 0x29, 0xa2, 0xa8, 0xb1, 0xf0, 0xb3, 0xc4, 
	0x30, 0x7e, 0x63, 0x38, 0xcb, 0xf4, 0x4f, 0xd1, 
	0xdf, 0x44, 0x32, 0xdc, 0x17, 0x5f, 0x66, 0x2a, 
	0x81, 0x9e, 0x77, 0x4a, 0x65, 0x67, 0x34, 0xfa, 
	0x54, 0x1e, 0x14, 0xbe, 0x04, 0xf1, 0xa7, 0x9c, 
	0x8b, 0x37, 0xee, 0x85, 0xab, 0x22, 0x0f, 0x69, 
	0xc5, 0xd4, 0x05, 0x84, 0xa4, 0x73, 0x42, 0xa1, 
	0x64, 0xe1, 0x70, 0x83, 0x90, 0xc2, 0x48, 0x0d, 
	0x61, 0x1c, 0xc6, 0x72, 0xfb, 0x76, 0x74, 0xe7, 
	0x01, 0xd8, 0xc8, 0xd2, 0x75, 0xa3, 0xcf, 0x28, 
	0x82, 0x1d, 0x49, 0x35, 0xc7, 0xbd, 0xca, 0xa6, 
	0xac, 0x0c, 0x62, 0xad, 0xf9, 0x3c, 0xea, 0x2d, 
	0x59, 0xda, 0x3e, 0x97, 0x6d, 0x09, 0xf7, 0x55, 
	0xe5, 0x23, 0x53, 0x9f, 0x06, 0xbc, 0x95, 0x78, 
};

static uint32_t rol32(uint32_t code, int n)
{
	return (code << n) | (code >> (32 - n));
}

static uint8_t rol8(uint8_t code, int n)
{
	return (code << n) | (code >> (8 - n));
}

static uint32_t F(uint32_t code, uint32_t key)
{
	uint32_t work;

	work = code + key;
	work ^= sbox_f[work & 0xff];
	work ^= rol32(work, 9) ^ rol32(work, 22);
	return work;
}

void c2_init()
{
	int i;
	unsigned char c0, c1, c2, c3;
	
	for (i = 0; i < 256; i++)
	{
		c0 = sbox[i];
		c1 = rol8((c0 ^ 0x65), 1);
		c2 = rol8((c0 ^ 0x2b), 5);
		c3 = rol8((c0 ^ 0xc9), 2);
		c0 ^= i;
		sbox_f[i] = (c3 << 24) + (c2 << 16) + (c1 << 8) + c0;
	}
}

uint64_t c2_enc(uint64_t code, uint64_t key)
{
	uint32_t L, R, t;
	uint32_t ktmpa, ktmpb, ktmpc, ktmpd;
	uint32_t sk[10];
	int      round;

	L     = (uint32_t)((code >> 32) & 0xffffffff);
	R     = (uint32_t)((code      ) & 0xffffffff);
	ktmpa = (uint32_t)((key  >> 32) & 0x00ffffff);
	ktmpb = (uint32_t)((key       ) & 0xffffffff);
	for (round = 0; round < 10; round++)
	{
		ktmpa &= 0x00ffffff;
		sk[round] = ktmpb + ((uint32_t)sbox[(ktmpa & 0xff) ^ round] << 4);
		ktmpc = (ktmpb >> (32 - 17));
		ktmpd = (ktmpa >> (24 - 17));
		ktmpa = (ktmpa << 17) | ktmpc;
		ktmpb = (ktmpb << 17) | ktmpd;
	}
	for (round = 0; round < 10; round++)
	{
		L += F(R, sk[round]);
		t = L; L = R; R = t;
	}
	t = L; L = R; R = t;
	return (((uint64_t)L) << 32) | R;
}

uint64_t c2_dec(uint64_t code, uint64_t key)
{
	uint32_t L, R, t;
	uint32_t ktmpa, ktmpb, ktmpc, ktmpd;
	uint32_t sk[10];
	int      round;

	L  =    (uint32_t)((code >> 32) & 0xffffffff);
	R  =    (uint32_t)((code      ) & 0xffffffff);
	ktmpa = (uint32_t)((key  >> 32) & 0x00ffffff);
	ktmpb = (uint32_t)((key       ) & 0xffffffff);
	for (round = 0; round < 10; round++)
	{
		ktmpa &= 0x00ffffff;
		sk[round] = ktmpb + ((uint32_t)sbox[(ktmpa & 0xff) ^ round] << 4);
		ktmpc = (ktmpb >> (32 - 17));
		ktmpd = (ktmpa >> (24 - 17));
		ktmpa = (ktmpa << 17) | ktmpc;
		ktmpb = (ktmpb << 17) | ktmpd;
	}
	for (round = 9; round >= 0; round--)
	{
		L -= F(R, sk[round]);
		t = L; L = R; R = t;
	}
	t = L; L = R; R = t;
	return (((uint64_t)L) << 32) | R;
}

uint64_t c2_g(uint64_t code, uint64_t key)
{
	return c2_enc(code, key) ^ code;
}

void c2_ecbc(void *p_buffer, uint64_t key, int length)
{
	uint32_t L, R, t;
	uint32_t ktmpa, ktmpb, ktmpc, ktmpd;
	uint32_t sk[10];
	uint64_t inout, inkey;
	int      round, key_round, i;

	inkey = key;
	key_round = 10;
	for (i = 0; i < length; i += 8)
	{
		inout = *(uint64_t*)p_buffer;
		B2N_64(inout);
		L  =    (uint32_t)((inout >> 32) & 0xffffffff);
		R  =    (uint32_t)((inout      ) & 0xffffffff);
		ktmpa = (uint32_t)((inkey >> 32) & 0x00ffffff);
		ktmpb = (uint32_t)((inkey      ) & 0xffffffff);
		for (round = 0; round < key_round; round++)
		{
			ktmpa &= 0x00ffffff;
			sk[round] = ktmpb + ((uint32_t)sbox[(ktmpa & 0xff) ^ round] << 4);
			ktmpc = (ktmpb >> (32 - 17));
			ktmpd = (ktmpa >> (24 - 17));
			ktmpa = (ktmpa << 17) | ktmpc;
			ktmpb = (ktmpb << 17) | ktmpd;
		}
		for (round = 0; round < 10; round++)
		{
			L += F(R, sk[round % key_round]);
			if (round == 4)
			{
				inkey = key ^ (((uint64_t)(R & 0x00ffffff) << 32) | L);
			}
			t = L; L = R; R = t;
		}
		t = L; L = R; R = t;
		inout = (((uint64_t)L) << 32) | R;
		B2N_64(inout);
		*((uint64_t*)p_buffer) = inout;
		p_buffer = (uint64_t*) p_buffer + 1;
		key_round = 2;
	}
}

void c2_dcbc(void *p_buffer, uint64_t key, int length)
{
	uint32_t L, R, t;
	uint32_t ktmpa, ktmpb, ktmpc, ktmpd;
	uint32_t sk[10];
	uint64_t inout, inkey;
	int      round, key_round, i;

	inkey = key;
	key_round = 10;
	for (i = 0; i < length; i += 8)
	{
		inout = *(uint64_t*)p_buffer;
		B2N_64(inout);
		L  =    (uint32_t)((inout >> 32) & 0xffffffff);
		R  =    (uint32_t)((inout      ) & 0xffffffff);
		ktmpa = (uint32_t)((inkey >> 32) & 0x00ffffff);
		ktmpb = (uint32_t)((inkey      ) & 0xffffffff);
		for (round = 0; round < key_round; round++)
		{
			ktmpa &= 0x00ffffff;
			sk[round] = ktmpb + ((uint32_t)sbox[(ktmpa & 0xff) ^ round] << 4);
			ktmpc = (ktmpb >> (32 - 17));
			ktmpd = (ktmpa >> (24 - 17));
			ktmpa = (ktmpa << 17) | ktmpc;
			ktmpb = (ktmpb << 17) | ktmpd;
		}
		for (round = 9; round >= 0; round--)
		{
			L -= F(R, sk[round % key_round]);
			t = L; L = R; R = t;
			if (round == 5)
			{
				inkey = key ^ (((uint64_t)(R & 0x00ffffff) << 32) | L);
			}
		}
		t = L; L = R; R = t;
		inout = (((uint64_t)L) << 32) | R;
		B2N_64(inout);
		*((uint64_t*)p_buffer) = inout;
		p_buffer = (uint64_t*) p_buffer + 1;
		key_round = 2;
	}
}
