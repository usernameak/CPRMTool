#include "cprmdrvr.h"

#include <stdint.h>
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/mmc/ioctl.h>
#include "linux_mmc_defs.h"
#endif

#include <string.h>
#include <errno.h>

#include "pgl_log.h"

#ifdef _WIN32
// #define USE_SFF_DRIVER
#define USE_REALTEK_DRIVER
#else
#define USE_MMCBLK_DRIVER
#endif

#ifdef USE_SFF_DRIVER
#include "sffdisk.h"
#include "sddef.h"
#endif

#ifdef _WIN32
static HANDLE gSDHandle;
static char gSDLocked = 0;

#define SCSI_IOCTL_DATA_OUT 0
#define SCSI_IOCTL_DATA_IN 1
#define SCSI_IOCTL_DATA_UNSPECIFIED 2

#define IOCTL_SCSI_PASS_THROUGH_DIRECT 0x4d014

typedef struct _SCSI_PASS_THROUGH_DIRECT {
    USHORT Length;
    UCHAR ScsiStatus;
    UCHAR PathId;
    UCHAR TargetId;
    UCHAR Lun;
    UCHAR CdbLength;
    UCHAR SenseInfoLength;
    UCHAR DataIn;
    ULONG DataTransferLength;
    ULONG TimeOutValue;
    PVOID DataBuffer;
    ULONG SenseInfoOffset;
    UCHAR Cdb[16];
} SCSI_PASS_THROUGH_DIRECT, *PSCSI_PASS_THROUGH_DIRECT;

#pragma pack(push, sensedata, 1)
typedef struct _SENSE_DATA {
    UCHAR ErrorCode : 7;
    UCHAR Valid     : 1;
    UCHAR SegmentNumber;
    UCHAR SenseKey        : 4;
    UCHAR Reserved        : 1;
    UCHAR IncorrectLength : 1;
    UCHAR EndOfMedia      : 1;
    UCHAR FileMark        : 1;
    UCHAR Information[4];
    UCHAR AdditionalSenseLength;
    UCHAR CommandSpecificInformation[4];
    UCHAR AdditionalSenseCode;
    UCHAR AdditionalSenseCodeQualifier;
    UCHAR FieldReplaceableUnitCode;
    UCHAR SenseKeySpecific[3];
} SENSE_DATA, *PSENSE_DATA;
#pragma pack(pop, sensedata)
#else
int gSDHandle = -1;
#endif

int cprm_drvr_init(const char *sdCardPath) {
#ifdef _WIN32
    gSDHandle = CreateFileA(
        sdCardPath,
        GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL);
    if (gSDHandle == INVALID_HANDLE_VALUE) {
        PGL_LOG("sd init failed with GetLastError()=%08x", GetLastError());
        return -1;
    }
#else
    gSDHandle = open(sdCardPath, O_RDONLY);
    if (gSDHandle == -1) {
        PGL_LOG("sd init failed with: %s", strerror(errno));
        return -1;
    }
#endif
    return 0;
}

#if defined(USE_REALTEK_DRIVER)
int cprm_drvr_pt(int enable) {
    int result = 0;

    char buffer[18];

    int reqSize   = sizeof(SCSI_PASS_THROUGH_DIRECT) + sizeof(SENSE_DATA);
    BYTE *request = malloc(reqSize);
    ZeroMemory(request, reqSize);

    SCSI_PASS_THROUGH_DIRECT *pPtd = (SCSI_PASS_THROUGH_DIRECT *)request;
    pPtd->Length                   = sizeof(SCSI_PASS_THROUGH_DIRECT);
    pPtd->CdbLength                = 10;
    pPtd->SenseInfoLength          = sizeof(SENSE_DATA);
    pPtd->DataIn                   = SCSI_IOCTL_DATA_IN;
    pPtd->DataTransferLength       = 18;
    pPtd->TimeOutValue             = 3;
    pPtd->DataBuffer               = buffer;
    pPtd->SenseInfoOffset          = sizeof(SCSI_PASS_THROUGH_DIRECT);

    SENSE_DATA *pSense = (SENSE_DATA *)(request + sizeof(SCSI_PASS_THROUGH_DIRECT));

    // 0 - scsi command id
    pPtd->Cdb[0] = 0xD0;

    // 1 - flags
    pPtd->Cdb[1] = enable ? 1 : 0;

    // 2 - `SD Card` string is required
    memcpy(&pPtd->Cdb[2], "SD Card", 7);

    DWORD nBytesReturned;
    if (!DeviceIoControl(
            gSDHandle,
            IOCTL_SCSI_PASS_THROUGH_DIRECT,
            request,
            reqSize,
            request,
            reqSize,
            &nBytesReturned,
            NULL)) {
        PGL_LOG("cprm_drvr_pt errno %08x", (unsigned int)GetLastError());
        result = -1;
    }

    if (pPtd->SenseInfoLength) {
        PGL_LOG("cprm_drvr_pt sense error");
        result = -1;
    }

    free(request);

    return result;
}
#endif

int cprm_drvr_sendcmd(int slot, unsigned int cmd, uint32_t arg, int numBlocks, int blockSize, uint8_t *buffer) {
    int length = numBlocks * blockSize;
#if defined(USE_SFF_DRIVER)
    DWORD nSizeOfCmd = sizeof(SFFDISK_DEVICE_COMMAND_DATA) + sizeof(SDCMD_DESCRIPTOR) + length;

    SFFDISK_DEVICE_COMMAND_DATA cmdData;
    cmdData.HeaderSize           = sizeof(SFFDISK_DEVICE_COMMAND_DATA);
    cmdData.Flags                = 0;
    cmdData.Command              = SFFDISK_DC_DEVICE_COMMAND;
    cmdData.ProtocolArgumentSize = sizeof(SDCMD_DESCRIPTOR);
    cmdData.DeviceDataBufferSize = length;
    cmdData.Information          = arg;

    SDCMD_DESCRIPTOR desc;
    desc.CmdClass = cmd & ACMD ? SDCC_APP_CMD : SDCC_STANDARD;
    desc.Cmd      = (UCHAR)(cmd & 0xFF);

    switch (GETRESP(cmd)) {
    case 0:
        desc.ResponseType = SDRT_NONE;
        break;
    case 1:
        desc.ResponseType = SDRT_1;
        break;
    case 2:
        desc.ResponseType = SDRT_1B;
        break;
    case 3:
        desc.ResponseType = SDRT_2;
        break;
    case 4:
        desc.ResponseType = SDRT_3;
        break;
    case 5:
        desc.ResponseType = SDRT_6;
        break;
    }
    desc.TransferDirection = (cmd & DIR_OUT) ? SDTD_WRITE : SDTD_READ;
    desc.TransferType      = (cmd & DT) ? SDTT_CMD_ONLY : SDTT_SINGLE_BLOCK;
    if (cmd == ACMD18 || cmd == ACMD25) {
        desc.TransferType = SDTT_MULTI_BLOCK;
    }

    BYTE *pCmdBuf    = malloc(nSizeOfCmd);
    BYTE *pCmdBufout = malloc(nSizeOfCmd);

    memset(pCmdBuf, 0, nSizeOfCmd);
    memcpy(pCmdBuf, &cmdData, sizeof(SFFDISK_DEVICE_COMMAND_DATA));
    memcpy(pCmdBuf + sizeof(SFFDISK_DEVICE_COMMAND_DATA), &desc, sizeof(SDCMD_DESCRIPTOR));
    memcpy(pCmdBuf + sizeof(SFFDISK_DEVICE_COMMAND_DATA) + sizeof(SDCMD_DESCRIPTOR), buffer, length);

    memset(pCmdBufout, 0, nSizeOfCmd);

    int result = 0;

    DWORD nBytesReturned;
    if (!DeviceIoControl(
            gSDHandle,
            IOCTL_SFFDISK_DEVICE_COMMAND,
            pCmdBuf,
            nSizeOfCmd,
            pCmdBufout,
            nSizeOfCmd,
            &nBytesReturned,
            NULL)) {
        PGL_LOG("cprm_drvr_sendcmd errno %08x", (unsigned int)GetLastError());
        result = -1;
    }

    PGL_LOG("cprm_drvr_sendcmd: nBytesReturned %lu", nBytesReturned);

    if (desc.TransferDirection == SDTD_READ) {
        PGL_LOG("cprm_drvr_sendcmd: copy data back");
        memcpy(buffer, pCmdBufout + sizeof(SFFDISK_DEVICE_COMMAND_DATA) + sizeof(SDCMD_DESCRIPTOR), length);
    } else {
        PGL_LOG("cprm_drvr_sendcmd: is write dir");
    }

    free(pCmdBuf);
    free(pCmdBufout);

    return result;
#elif defined(USE_REALTEK_DRIVER)
    int result = 0;

    result = cprm_drvr_pt(1);
    if (result) return result;

    int reqSize   = sizeof(SCSI_PASS_THROUGH_DIRECT) + sizeof(SENSE_DATA);
    BYTE *request = malloc(reqSize);
    ZeroMemory(request, reqSize);

    SCSI_PASS_THROUGH_DIRECT *pPtd = (SCSI_PASS_THROUGH_DIRECT *)request;
    pPtd->Length                   = sizeof(SCSI_PASS_THROUGH_DIRECT);
    pPtd->CdbLength                = 12;
    pPtd->SenseInfoLength          = sizeof(SENSE_DATA);
    pPtd->DataIn                   = (cmd & DIR_OUT) ? SCSI_IOCTL_DATA_OUT : SCSI_IOCTL_DATA_IN;
    pPtd->DataTransferLength       = length;
    pPtd->TimeOutValue             = 3;
    pPtd->DataBuffer               = buffer;
    pPtd->SenseInfoOffset          = sizeof(SCSI_PASS_THROUGH_DIRECT);

    SENSE_DATA *pSense = (SENSE_DATA *)(request + sizeof(SCSI_PASS_THROUGH_DIRECT));

    // 0 - scsi command id
    pPtd->Cdb[0] = 0xD1;
    if (cmd & DT) {
        if (cmd & DIR_OUT) {
            pPtd->Cdb[0] = 0xD3;
        } else {
            pPtd->Cdb[0] = 0xD2;
        }
    }

    // 1 - flags
    pPtd->Cdb[1] = 0;
    if (cmd & ACMD) {
        pPtd->Cdb[1] |= 0x1; // ACMD flag
    }
    // if (cmd == ACMD18 || cmd == ACMD25) {
    //     pPtd->Cdb[1] |= 0x4; // send CMD12
    // }

    // 2 - command id
    pPtd->Cdb[2] = cmd & 0x3F;

    // 3,4,5,6 - big-endian arg
    pPtd->Cdb[3] = (arg >> 24) & 0xFFu;
    pPtd->Cdb[4] = (arg >> 16) & 0xFFu;
    pPtd->Cdb[5] = (arg >> 8) & 0xFFu;
    pPtd->Cdb[6] = arg & 0xFFu;

    // 7,8,9 - big-endian length
    pPtd->Cdb[7] = (length >> 16) & 0xFFu;
    pPtd->Cdb[8] = (length >> 8) & 0xFFu;
    pPtd->Cdb[9] = length & 0xFFu;

    pPtd->Cdb[10] = 4; // i don't know what's this

    DWORD nBytesReturned;
    if (!DeviceIoControl(
            gSDHandle,
            IOCTL_SCSI_PASS_THROUGH_DIRECT,
            request,
            reqSize,
            request,
            reqSize,
            &nBytesReturned,
            NULL)) {
        PGL_LOG("cprm_drvr_sendcmd errno %08x", (unsigned int)GetLastError());
        result = -1;
    }

    if (pPtd->SenseInfoLength) {
        PGL_LOG("cprm_drvr_sendcmd sense error");
        result = -1;
    }

    int disRes = cprm_drvr_pt(0);
    if (!result) {
        result = disRes;
    }

    free(request);

    return result;
#elif defined(USE_MMCBLK_DRIVER)
    struct mmc_ioc_cmd cmdData;
    memset(&cmdData, 0, sizeof(cmdData));

    cmdData.write_flag = (cmd & DIR_OUT) ? 1 : 0;
    cmdData.is_acmd    = (cmd & ACMD) ? 1 : 0;
    cmdData.opcode     = cmd & 0x3F;
    cmdData.arg        = arg;
    switch (GETRESP(cmd)) {
    case 0:
        cmdData.flags = MMC_RSP_NONE;
        break;
    case 1:
        cmdData.flags = MMC_RSP_R1 | MMC_RSP_SPI_R1;
        break;
    case 2:
        cmdData.flags = MMC_RSP_R1B | MMC_RSP_SPI_R1B;
        break;
    case 3:
        cmdData.flags = MMC_RSP_R2 | MMC_RSP_SPI_R2;
        break;
    case 4:
        cmdData.flags = MMC_RSP_R3 | MMC_RSP_SPI_R3;
        break;
    case 5:
        cmdData.flags = MMC_RSP_R6;
        break;
    }
    if (cmd & DT) {
        cmdData.flags |= MMC_CMD_ADTC;
    } else {
        cmdData.flags |= MMC_CMD_AC;
    }
    cmdData.blksz = blockSize;
    cmdData.blocks = numBlocks;
    mmc_ioc_cmd_set_data(cmdData, buffer);

    int result = ioctl(gSDHandle, MMC_IOC_CMD, &cmdData);
    if (result == -1) {
        PGL_LOG("cprm_drvr_sendcmd errno %08x", errno);
        return -1;
    }

    return 0;
#else
    return -1;
#endif
}

int cprm_drvr_terminate(void) {
#ifdef _WIN32
    CloseHandle(gSDHandle);
#else
    close(gSDHandle);
#endif
    return 0;
}
