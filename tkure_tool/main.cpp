//                                                                  //
//                                                                  //
// WARNING: THIS CODE IS EXPERIMENTAL AND WILL MOST LIKELY NOT WORK //
//                                                                  //
//                                                                  //

#include <cstdio>
#include <cstdint>
#include <c2.h>
#include <mkb.h>
#include <bswap.h>
#include <cctype>

class FileReader {
    FILE *m_fp;

public:
    FileReader() : m_fp(nullptr) {}

    ~FileReader() {
        if (m_fp) fclose(m_fp);
    }

    bool open(const char *filename) {
        m_fp = fopen(filename, "rb");
        return m_fp != nullptr;
    }

    void read(uint8_t &val) {
        fread(&val, 1, sizeof(val), m_fp);
    }

    void read(uint16_t &val) {
        fread(&val, 1, sizeof(val), m_fp);
        B2N_16(val);
    }

    void read(uint32_t &val) {
        fread(&val, 1, sizeof(val), m_fp);
        B2N_32(val);
    }

    void read(uint8_t *arr, uint32_t size) {
        fread(arr, 1, size, m_fp);
    }

    void skip(uint32_t size) {
        fseek(m_fp, size, SEEK_CUR);
    }
};


void hexdump(void *ptr, int buflen) {
    unsigned char *buf = (unsigned char *)ptr;
    int i, j;
    for (i = 0; i < buflen; i += 16) {
        printf("%06x: ", i);
        for (j = 0; j < 16; j++)
            if (i + j < buflen)
                printf("%02x ", buf[i + j]);
            else
                printf("   ");
        printf(" ");
        for (j = 0; j < 16; j++)
            if (i + j < buflen)
                printf("%c", isprint(buf[i + j]) ? buf[i + j] : '.');
        printf("\n");
    }
}

int main() {
    c2_init();

    FileReader mkbRd{};
    if (!mkbRd.open("mkb11.bin")) {
        fprintf(stderr, "failed to open mkb11.bin");
        return 1;
    }
    auto *mkbData = new uint8_t[65536];
    mkbRd.read(mkbData, 65536);

    uint64_t mediaKey;
    if (cprm_process_mkb(mkbData, cprm_device_keychains[11], 16, &mediaKey) == -1) {
        fprintf(stderr, "failed to process mkb");
        return 1;
    }

    FileReader rd{};
    bool status = rd.open("BIND0001.KEY");
    if (!status) {
        fprintf(stderr, "failed to open BIND0001.KEY");
        return 1;
    }

    bool tkure_used[256];

    for (uint32_t i = 0; i < 32; i++) {
        uint8_t mask;
        rd.read(mask);
        for (uint32_t subidx = 0; subidx < 8; subidx++) {
            bool used                  = (mask >> (7 - subidx)) & 1;
            tkure_used[i * 8 + subidx] = used;
            if (used) {
                printf("TKURE %d used\n", i * 8 + subidx);
            }
        }
    }
    rd.skip(352);

    for (uint32_t i = 0; i < 250; i++) {
        uint8_t tkure[64];
        rd.read(tkure, 64);
        c2_dcbc(tkure, 0x88FCD51F5A3181, 64);
        if (i == 0) {
            hexdump(tkure, 64);
        }
    }

    return 0;
}