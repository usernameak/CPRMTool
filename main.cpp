#include <cstdlib>
#include <cstring>
#include <array>
#include <climits>
#include <cerrno>

#include "c2lib/bswap.h"

extern "C" {
#include "cprmdrvr.h"
}

#include <c2.h>
#include <mkb.h>

#include "pgl_log.h"

#define LOW_56_BITS(x) ((x)&0xFFFFFFFFFFFFFFull)

// #define ENABLE_FLASHING_SUPPORT

int cprm_get_media_key_block(uint8_t *data, int mkbid) {
    PGL_LOG("reading MKB data...");
    memset(data, 0, 65536);
    for (int i = 0; i < 128; i++) {
        int err = cprm_drvr_sendcmd(0, 0x8C2Bu, i | 0x1000000 | (mkbid << 16), 1, 512, data + 512 * i);
        if (err) {
            PGL_LOG("drvr sendcmd error %d", err);

            return err;
        }
    }
    return 0;
}

int cprm_get_media_key_from_sd(int mkbIndex, uint64_t *poutMediaKey) {
    uint8_t *mkbData = new uint8_t[65536];
    int err          = cprm_get_media_key_block(mkbData, mkbIndex);
    if (err) {
        delete[] mkbData;
        return -1;
    }

    PGL_LOG("decrypting media key...");

    if (mkbIndex >= 16 || !cprm_device_keychains[mkbIndex]) {
        PGL_LOG("could not find a valid keychain for MKB%d", mkbIndex);
    }

    err = cprm_process_mkb(mkbData, cprm_device_keychains[mkbIndex], 16, poutMediaKey);
    if (err) {
        delete[] mkbData;
        return err;
    }

    delete[] mkbData;

    return 0;
}

int cprm_get_media_id_from_sd(uint64_t *poutMediaId) {
    PGL_LOG("reading media id...");

    uint64_t media_id;
    int err = cprm_drvr_sendcmd(0, 0x8C2Cu, 0, 1, 8, (uint8_t *)&media_id);
    if (err) {
        return err;
    }
    B2N_64(media_id);
    *poutMediaId = media_id;
    return 0;
}

uint64_t cprm_secure_random() {
    return 0xDEADBEEFCAFEBABEull;
}

int cprm_do_ake(uint64_t k_mu, uint32_t arg, uint64_t *poutSessionKey) {
    uint64_t ch1p = ((uint64_t)arg << 32) | (cprm_secure_random() & 0xFFFFFFFF);
    uint64_t ch1  = c2_enc(ch1p, k_mu);
    // PGL_LOG("ch1 = %016llx", ch1);
    uint64_t ch1s = ch1;
    B2N_64(ch1s);

    int err = cprm_drvr_sendcmd(0, 0xCC2Du, 0, 1, 8, (uint8_t *)&ch1s);
    if (err) {
        PGL_LOG("setchlg: drvr sendcmd error %d", err);

        return err;
    }

    uint64_t ch2;
    err = cprm_drvr_sendcmd(0, 0x8C2Eu, 0, 1, 8, (uint8_t *)&ch2);
    if (err) {
        PGL_LOG("getchlg: drvr sendcmd error %d", err);

        return err;
    }
    B2N_64(ch2);
    // PGL_LOG("ch2 = %016llx", ch2);

    uint64_t res2 = c2_g(ch2, k_mu);
    B2N_64(res2);

    err = cprm_drvr_sendcmd(0, 0xCC2Fu, 0, 1, 8, (uint8_t *)&res2);
    if (err) {
        PGL_LOG("setresp: drvr sendcmd error %d", err);

        return err;
    }

    // PGL_LOG("res2 = %016llx", res2);

    uint64_t res1;
    err = cprm_drvr_sendcmd(0, 0x8C30u, 0, 1, 8, (uint8_t *)&res1);
    if (err) {
        PGL_LOG("getresp: drvr sendcmd error %d", err);

        return err;
    }
    B2N_64(res1);

    // PGL_LOG("res1 = %016llx", res1);

    uint64_t vres1 = c2_g(ch1, k_mu);
    // PGL_LOG("vres1 = %016llx", vres1);
    if (vres1 != res1) {
        PGL_LOG("getresp: vres1 != res1");

        return err;
    }
    // PGL_LOG("AKE success");

    uint64_t session_key = LOW_56_BITS(c2_g(ch1 ^ ch2, LOW_56_BITS(~k_mu)));

    *poutSessionKey = session_key;

    return 0;
}

void DumpHex(const void *data, size_t size) {
    char ascii[17];
    size_t i, j;
    ascii[16] = '\0';
    for (i = 0; i < size; ++i) {
        printf("%02X ", ((unsigned char *)data)[i]);
        if (((unsigned char *)data)[i] >= ' ' && ((unsigned char *)data)[i] <= '~') {
            ascii[i % 16] = ((unsigned char *)data)[i];
        } else {
            ascii[i % 16] = '.';
        }
        if ((i + 1) % 8 == 0 || i + 1 == size) {
            printf(" ");
            if ((i + 1) % 16 == 0) {
                printf("|  %s \n", ascii);
            } else if (i + 1 == size) {
                ascii[(i + 1) % 16] = '\0';
                if ((i + 1) % 16 <= 8) {
                    printf(" ");
                }
                for (j = (i + 1) % 16; j < 16; ++j) {
                    printf("   ");
                }
                printf("|  %s \n", ascii);
            }
        }
    }
}

int cprm_read_protected_sector(uint64_t k_mu, uint32_t sectorNumber, uint32_t numSectors, uint8_t *data, uint64_t *pSessionKey) {
    PGL_LOG("reading PA sector 0x%08x...", sectorNumber);
    uint32_t arg = sectorNumber | (numSectors << 24);
    uint64_t session_key;

    int err = cprm_do_ake(k_mu, arg, &session_key);
    if (err) {
        fprintf(stderr, "cprm_do_ake failed\n");
        return -1;
    }
    // PGL_LOG("session key %016llx", session_key);

    err = cprm_drvr_sendcmd(0, 0x8C12u, 0, numSectors, 512, data);
    if (err) {
        fprintf(stderr, "parea read: sendcmd failed %d\n", err);
        return -1;
    }

    c2_dcbc(data, session_key, numSectors * 512);
    *pSessionKey = session_key;

    return 0;
}

int cprm_write_protected_sector(uint64_t k_mu, uint32_t sectorNumber, uint32_t numSectors, uint8_t *data) {
    PGL_LOG("writing PA sector 0x%08x...", sectorNumber);
    uint32_t arg = sectorNumber | (numSectors << 24);
    uint64_t session_key;

    int err = cprm_do_ake(k_mu, arg, &session_key);
    if (err) {
        fprintf(stderr, "cprm_do_ake failed\n");
        return -1;
    }

    return 0;
}

enum CPRMToolMode {
    CPRMTOOL_MODE_UNKNOWN,
    CPRMTOOL_MODE_DUMPMKB,
    CPRMTOOL_MODE_DUMPMID,
    CPRMTOOL_MODE_DUMPPA,
    CPRMTOOL_MODE_FLASHPA,
};

int main(int argc, char **argv) {

    c2_init();

    CPRMToolMode mode        = CPRMTOOL_MODE_UNKNOWN;
    int mkbid                = -1;
    const char *sdCardPath   = NULL;
    const char *outputPath   = NULL;
    const char *skOutputPath = NULL;
    const char *inputPath    = NULL;
    bool slowMode            = false;

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-dumpmkb") == 0) {
            if (mode != CPRMTOOL_MODE_UNKNOWN) {
                fprintf(stderr, "You cannot specify multiple modes.\n");
            }
            mode = CPRMTOOL_MODE_DUMPMKB;
        } else if (strcmp(argv[i], "-dumpmid") == 0) {
            if (mode != CPRMTOOL_MODE_UNKNOWN) {
                fprintf(stderr, "You cannot specify multiple modes.\n");
            }
            mode = CPRMTOOL_MODE_DUMPMID;
        } else if (strcmp(argv[i], "-dumppa") == 0) {
            if (mode != CPRMTOOL_MODE_UNKNOWN) {
                fprintf(stderr, "You cannot specify multiple modes.\n");
            }
            mode = CPRMTOOL_MODE_DUMPPA;
        } else if (strcmp(argv[i], "-flashpa") == 0) {
            if (mode != CPRMTOOL_MODE_UNKNOWN) {
                fprintf(stderr, "You cannot specify multiple modes.\n");
            }
            mode = CPRMTOOL_MODE_FLASHPA;
        } else if (strcmp(argv[i], "-mkbid") == 0) {
            i++;
            if (i >= argc) {
                fprintf(stderr, "-mkbid: Invalid argument.\n");
            }
            char *endptr;
            long mkbidL = strtol(argv[i], &endptr, 10);
            if (endptr == argv[i] || ((mkbidL == LONG_MAX || mkbidL == LONG_MIN) && errno == ERANGE) || mkbidL < 0 || mkbidL >= 16) {
                fprintf(stderr, "-mkbid: Invalid argument.\n");
            }
            mkbid = (int)mkbidL;
        } else if (strcmp(argv[i], "-card") == 0) {
            i++;
            if (i >= argc) {
                fprintf(stderr, "-card: Invalid argument.\n");
            }
            sdCardPath = argv[i];
        } else if (strcmp(argv[i], "-out") == 0) {
            i++;
            if (i >= argc) {
                fprintf(stderr, "-out: Invalid argument.\n");
            }
            outputPath = argv[i];
        } else if (strcmp(argv[i], "-skout") == 0) {
            i++;
            if (i >= argc) {
                fprintf(stderr, "-skout: Invalid argument.\n");
            }
            skOutputPath = argv[i];
        } else if (strcmp(argv[i], "-in") == 0) {
            i++;
            if (i >= argc) {
                fprintf(stderr, "-in: Invalid argument.\n");
            }
            inputPath = argv[i];
        } else if (strcmp(argv[i], "-slowmode") == 0) {
            slowMode = true;
        } else {
            fprintf(stderr, "Unknown option %s\n", argv[i]);
        }
    }

    if (mode == CPRMTOOL_MODE_UNKNOWN) {
        fprintf(stderr, "usage: CPRMTool <mode> <options>\n\n");
        fprintf(stderr, "<mode>:\n");
        fprintf(stderr, "  -dumpmkb - dumps the Media Key Block from SD Card\n");
        fprintf(stderr, "  -dumpmid - dumps the Media ID from SD Card\n");
        fprintf(stderr, "    note: the ID is saved as little-endian, despite\n");
        fprintf(stderr, "    being reported by card as big-endian.\n");
        fprintf(stderr, "  -dumppa - dumps the Protected Area from SD Card\n");
#ifdef ENABLE_FLASHING_SUPPORT
        fprintf(stderr, "  -flashpa - flashes the Protected Area into SD Card\n");
#endif
        fprintf(stderr, "\n");
        fprintf(stderr, "<options>:\n");
        fprintf(stderr, "  -card - required, selects the card, e.g. \\\\.\\PHYSICALDRIVE1\n");
        fprintf(stderr, "  -slowmode - slow mode. might be necessary for some quirky cards,"
                        "    use this if you get corrupted data\n");
        fprintf(stderr, "  -mkbid - for dumpmkb mode, selects MKB ID to be dumped,\n"
                        "    for other modes selects the keychain to be used\n");
        fprintf(stderr, "  -out - required (except for -flashpa), output file for the dump, e.g. data.bin\n");
        fprintf(stderr, "  -skout - required for dumppa, output file to dump session keys\n");
        fprintf(stderr, "    in the current -dumppa session, e.g. session_keys.bin\n");
        fprintf(stderr, "  -in - required for -flashpa, input file to flash, e.g. data.bin\n");
        fprintf(stderr, "\n");
        return 2;
    }

    if (sdCardPath == NULL) {
        fprintf(stderr, "-card not specified, aborting.");
        return 3;
    }

    if (outputPath == NULL && mode != CPRMTOOL_MODE_FLASHPA) {
        fprintf(stderr, "-out not specified, aborting.");
        return 4;
    }

    if (inputPath == NULL && mode == CPRMTOOL_MODE_FLASHPA) {
        fprintf(stderr, "-in not specified, aborting.");
        return 4;
    }

    if (mode == CPRMTOOL_MODE_DUMPMKB && mkbid < 0) {
        fprintf(stderr, "-mkbid not specified, aborting.");
        return 5;
    }

    int err = cprm_drvr_init(sdCardPath);
    if (err) {
        printf("drvr init error %d\n", err);
        return 1;
    }

    uint64_t media_id = 0xCCCCCCCCCCCCCCCCull;
    if (mode == CPRMTOOL_MODE_DUMPMID || mode == CPRMTOOL_MODE_DUMPPA) {
        err = cprm_get_media_id_from_sd(&media_id);
        if (err) {
            fprintf(stderr, "cprm_get_media_id_from_sd failed\n");
            cprm_drvr_terminate();
            return 1;
        }
        printf("media_id %016llx\n", media_id);
    }

    if (mode == CPRMTOOL_MODE_DUMPPA) {
        if (mkbid == -1) mkbid = 11; // default to MKB11 for protected area dumping

        uint64_t media_key;
        err = cprm_get_media_key_from_sd(mkbid, &media_key);
        if (err) {
            fprintf(stderr, "cprm_get_media_key_from_sd failed\n");
            cprm_drvr_terminate();
            return 1;
        }
        printf("media_key %016llx\n", media_key);

        uint64_t k_mu = c2_g(media_id, media_key) & 0xFFFFFFFFFFFFFFull;
        printf("K_mu %016llx\n", k_mu);

        FILE *fp   = fopen(outputPath, "wb");
        FILE *skfp = NULL;
        if (skOutputPath) {
            skfp = fopen(skOutputPath, "wb");
        }

        size_t sectorsPerRun = slowMode ? 1 : 64;
        uint8_t *sectorData  = new uint8_t[512 * sectorsPerRun];
        uint64_t sessionKey;
        for (int i = 0; i < 262144; i += sectorsPerRun) {
            memset(sectorData, 0xCC, 512 * sectorsPerRun);
            err = cprm_read_protected_sector(k_mu, i, sectorsPerRun, sectorData, &sessionKey);
            if (err) {
                fprintf(stderr, "cprm_read_protected_sector failed\n");
                fclose(fp);
                if (skfp) fclose(skfp);
                cprm_drvr_terminate();
                delete[] sectorData;
                return 1;
            }
            fwrite(sectorData, 1, 512 * sectorsPerRun, fp);
            if (skfp) fwrite(&sessionKey, sizeof(sessionKey), 1, skfp);
        }
        delete[] sectorData;

        if (skfp) fclose(skfp);
        fclose(fp);
    } else if (mode == CPRMTOOL_MODE_DUMPMID) {
        FILE *fp = fopen(outputPath, "wb");
        fwrite(&media_id, 1, sizeof(media_id), fp);
        fclose(fp);
    } else if (mode == CPRMTOOL_MODE_DUMPMKB) {
        uint8_t *mkbData = new uint8_t[65536];
        err              = cprm_get_media_key_block(mkbData, mkbid);
        if (err) {
            fprintf(stderr, "cprm_get_media_key_block failed\n");
            cprm_drvr_terminate();
            delete[] mkbData;
            return 1;
        }
        FILE *fp = fopen(outputPath, "wb");
        fwrite(mkbData, 1, 65536, fp);
        fclose(fp);
        delete[] mkbData;
    }
#ifdef ENABLE_FLASHING_SUPPORT
    else if (mode == CPRMTOOL_MODE_FLASHPA) {
        uint64_t media_key;
        err = cprm_get_media_key_from_sd(&media_key);
        if (err) {
            fprintf(stderr, "cprm_get_media_key_from_sd failed\n");
            cprm_drvr_terminate();
            return 1;
        }
        printf("media_key %016llx\n", media_key);

        uint64_t k_mu = c2_g(media_id, media_key) & 0xFFFFFFFFFFFFFFull;
        printf("K_mu %016llx\n", k_mu);

        uint8_t *sectorData = new uint8_t[512 * 64];
        int idSector        = 0;
        while (true) {
            memset(sectorData, 0xCC, 512 * 64);
            err = cprm_write_protected_sector(k_mu, i, 64, sectorData);
            if (err) {
                fprintf(stderr, "cprm_read_protected_sector failed\n");
                fclose(fp);
                cprm_drvr_terminate();
                delete[] sectorData;
                return 1;
            }
            fwrite(sectorData, 1, 512 * 64, fp);
            idSector += 64;
        }
        delete[] sectorData;
    }
#endif

    cprm_drvr_terminate();
    return 0;
}
